﻿Shader "Unlit/DepthViewShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _ParticleTex("Particle texture", 2D) = "white" {}
        _SquardSizeMult("Squard size multiplyer", Float) = 1
    }
        SubShader
    {
        Tags { "RenderType" = "Opaque" }
        //Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
        //ZWrite Off
        //Blend SrcAlpha OneMinusSrcAlpha
        LOD 100

        Pass
        {
            Lighting Off
            //Cull Off

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma geometry geom

            #include "UnityCG.cginc"
            

            uniform float _SquardSizeMult = 1.0f;

            struct appdata
            {
                float4 vertex   : POSITION;
                float3 normal   : NORMAL;
                float2 uv       : TEXCOORD0;
                float2 uv2      : TEXCOORD1;
                float2 uv3      : TEXCOORD2;
                float2 uv4      : TEXCOORD3;
                uint vertexId   : SV_VertexID;
            };


            struct v2g {
                float4 objPos   : SV_POSITION; // vertex object space position
                float3 normal   : NORMAL;
                float2 uv       : TEXCOORD0;
                float2 uv2      : TEXCOORD1;
                float2 uv3      : TEXCOORD2;
                float2 uv4      : TEXCOORD3;
                //uint vertexId   : SV_VertexID;
            };

            struct g2f {
                float4 worldPos : SV_POSITION;
                float2 uv       : TEXCOORD0;
                float2 particle_uv  : TEXCOORD4;
            };

            //struct g2f
            //{
            //    float2 uv : TEXCOORD0;
            //    UNITY_FOG_COORDS(1)
            //    float4 vertex : SV_POSITION;
            //    float pointSize : PSIZE;
            //};

            sampler2D _MainTex;
            sampler2D _ParticleTex;

            float4 _MainTex_ST;

            v2g vert (appdata data)
            {
                v2g out_v2f;

                //float4 worldPosition = data.vertex;
                //float4 viewPosition = mul(worldPosition, View);

                out_v2f.objPos = data.vertex;// UnityObjectToClipPos(data.vertex);
                out_v2f.normal = data.normal;
                out_v2f.uv  = data.uv;// TRANSFORM_TEX(data.uv, _MainTex);
                out_v2f.uv2 = data.uv2;
                out_v2f.uv3 = data.uv3;
                out_v2f.uv4 = data.uv4;

                //out_v2f.normal = UnityObjectToWorldNormal(data.normal);
                //UNITY_TRANSFER_FOG(o,o.vertex);
                //out_v2f.vertexId = data.vertexId;
                return out_v2f;
            }

            float4 make_offset(in float2 pos, in float2 offset) {
                float4 viewPos = mul(pos, UNITY_MATRIX_V);
                //float4 offseted = viewPosition.xy + float4(offset.x, offset.y, 0, 0);
                viewPos.x += offset.x;
                viewPos.y += offset.y;
                return  mul(viewPos, UNITY_MATRIX_P);
            } 

            const uint nPointHor = 512 / 4;
            const uint nPointVer = 424 / 4;

            [maxvertexcount(6)]
            void geom(point v2g input[1], inout TriangleStream<g2f> triangleStream) {

                // в normal хранится не нормаль а размер квадрата
                float2 squardSize = _SquardSizeMult * float2(input[0].normal.x, input[0].normal.y);

                // размер квадратика
                

                float4 topLeftPos  = input[0].objPos ;
                float4 topRightPos = input[0].objPos ;
                float4 botLeftPos  = input[0].objPos ;
                float4 botRightPos = input[0].objPos ;

                g2f topL;
                g2f topR;
                g2f botL;
                g2f botR;

                
                //topL.worldPos = mul(UNITY_MATRIX_MV, float4(topLeftPos.xyz, 1.0))   + float4(-squardSize.x,  squardSize.y, 0, 0);//
                //topR.worldPos = mul(UNITY_MATRIX_MV, float4(topRightPos.xyz, 1.0))  + float4( squardSize.x,  squardSize.y, 0, 0);//
                //botL.worldPos = mul(UNITY_MATRIX_MV, float4(botLeftPos.xyz, 1.0))   + float4(-squardSize.x, -squardSize.y, 0, 0);//
                //botR.worldPos = mul(UNITY_MATRIX_MV, float4(botRightPos.xyz, 1.0))  + float4( squardSize.x, -squardSize.y, 0, 0);//
                //
                topL.worldPos = mul(UNITY_MATRIX_MV, float4(topLeftPos.xyz, 1.0))  + float4(0, 0, 0, 0);//
                topR.worldPos = mul(UNITY_MATRIX_MV, float4(topRightPos.xyz, 1.0)) + float4( squardSize.x, 0, 0, 0);//
                botL.worldPos = mul(UNITY_MATRIX_MV, float4(botLeftPos.xyz, 1.0))  + float4(0, -squardSize.y, 0, 0);//
                botR.worldPos = mul(UNITY_MATRIX_MV, float4(botRightPos.xyz, 1.0)) + float4(squardSize.x, -squardSize.y, 0, 0);//

               
                // переводим координаты в пространство view и добавляем смещение
                /*
                float psize = _PointSize;//  0.025f;
                topL.worldPos = mul(UNITY_MATRIX_MV, float4(topLeftPos.xyz, 1.0))  + float4(-psize, psize, 0, 0);
                topR.worldPos = mul(UNITY_MATRIX_MV, float4(topRightPos.xyz, 1.0)) + float4(psize, psize, 0, 0);
                botL.worldPos = mul(UNITY_MATRIX_MV, float4(botLeftPos.xyz, 1.0))  + float4(-psize, -psize, 0, 0);
                botR.worldPos = mul(UNITY_MATRIX_MV, float4(botRightPos.xyz, 1.0)) + float4(psize, -psize, 0, 0);
                */
                // переводим координаты в пространство projection камеры
                topL.worldPos = mul(UNITY_MATRIX_P, float4(topL.worldPos.xyz, 1.0));
                topR.worldPos = mul(UNITY_MATRIX_P, float4(topR.worldPos.xyz, 1.0));
                botL.worldPos = mul(UNITY_MATRIX_P, float4(botL.worldPos.xyz, 1.0));
                botR.worldPos = mul(UNITY_MATRIX_P, float4(botR.worldPos.xyz, 1.0));


                // uv
                float2 uv_center = (input[0].uv + input[0].uv2 + input[0].uv3 + input[0].uv4) / 4.0f;


                //topL.uv = uv_center + (uv_center - input[0].uv ) * squardSize.x;
                //topR.uv = uv_center + (uv_center - input[0].uv2) * squardSize.x;
                //botL.uv = uv_center + (uv_center - input[0].uv3) * squardSize.x;
                //botR.uv = uv_center + (uv_center - input[0].uv4) * squardSize.x;

                topL.uv = input[0].uv;
                topR.uv = input[0].uv2;
                botL.uv = input[0].uv3;
                botR.uv = input[0].uv4;


                topL.particle_uv = float2(1.0f, 0.0f);
                topR.particle_uv = float2(1.0f, 1.0f);
                botL.particle_uv = float2(0.0f, 0.0f);
                botR.particle_uv = float2(0.0f, 1.0f);


                // кладем треугольник в поток
                triangleStream.Append(topL);
                triangleStream.Append(topR);
                triangleStream.Append(botL);
                triangleStream.RestartStrip();

                // кладем второй треугольник в поток
                triangleStream.Append(botL);
                triangleStream.Append(topR);
                triangleStream.Append(botR);
                triangleStream.RestartStrip();
            } 

            fixed4 frag(g2f i) : SV_Target
            {
                // sample the texture
                float4 particleColor = tex2D(_ParticleTex, i.particle_uv);
                float4 color = tex2D(_MainTex, i.uv);

                fixed4 col = color;

                //if (particleColor.a <= 0.99) discard;
                //col.a = particleColor.a;
                return col;
            }
            ENDCG
        }
    }
}
