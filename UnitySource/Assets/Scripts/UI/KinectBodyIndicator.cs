﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using Windows.Kinect;


/// <summary>
/// ui element to display body tracking status
/// </summary>
public class KinectBodyIndicator : MonoBehaviour
{
    /// <summary>GameObject with KinectDataSource script attached <see cref="KinectDataSource"/></summary>
    [Tooltip("GameObject with attached KinectDataSource script")]
    public GameObject _Kinect;
    KinectDataSouce kinectDataSource;


    /// <summary>
    /// map ui element to joint type
    /// </summary>
    public Dictionary<JointType, Image> images = new Dictionary<JointType, Image>();


    /// <summary>
    /// initialize ui
    /// </summary>
    void Start() {
        { // check kinect
            if(_Kinect == null) throw new Exception("no kinect body source provided");

            kinectDataSource = _Kinect.GetComponent<KinectDataSouce>();
            if(kinectDataSource == null) throw new Exception("given gameobject is not a kinect body source");
        }

        { // get Image components
            foreach(Transform child in transform) {
                Image image = child.GetComponent<Image>();
                if(image == null) continue;

                JointType jointType = (JointType) Enum.Parse(typeof(JointType), child.name, true);
                if(Enum.IsDefined(typeof(JointType), jointType)) {
                    images.Add(jointType, image);
                } else {
                    Debug.LogError("coud not convert name to JointType:" + child.name);
                    continue;
                }
            }
        }
    }


    // Update status of joints in indicator
    void Update()
    {
        if(kinectDataSource == null) return;

        Body trackingBody = null;

        // use first tracked body
        foreach(Body body in kinectDataSource.bodyData) {
            if(body == null) { continue; }
            if(body.IsTracked) { trackingBody = body; break; }
        }

        if(trackingBody == null) return; // no tracked body for now


        foreach(var pair in images) {
            JointType jointType = pair.Key;
            Image image = pair.Value;

            Color color;
            switch(trackingBody.Joints[jointType].TrackingState) {
                case TrackingState.Tracked:  color = Color.green; break;
                case TrackingState.Inferred: color = Color.red;   break;
                default: color = Color.blue; break;
            }

            image.color = color;
        }
    }
}
