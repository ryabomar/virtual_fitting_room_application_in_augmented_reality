﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// provides global features as close application
/// </summary>
public class ApplicationController : MonoBehaviour
{

    /// <summary>
    /// closes application then invoked
    /// </summary>
    public void exit() {
        Application.Quit();
    }
}
