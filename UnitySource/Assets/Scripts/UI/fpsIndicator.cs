﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// ui element shows current frames per second rate
/// </summary>
[RequireComponent(typeof(Text))]
public class fpsIndicator : MonoBehaviour
{
    Text textComponent;

    void Start() {
        textComponent = GetComponent<Text>();
    }

    /// <summary>
    /// update text field
    /// </summary>
    void Update() {
        textComponent.text = "FPS: " + (1.0f / Time.smoothDeltaTime).ToString("0.0");
    }
}
