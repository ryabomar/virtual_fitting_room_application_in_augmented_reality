﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;


/// <summary>
/// UI element controller to show time remaining before avatar adjustment begins
/// </summary>
public class AdjustBodyCountDown : MonoBehaviour
{

    /// <summary>
    /// countdown time
    /// </summary>
    public float time = 5.0f;

    /// <summary>
    /// avatar controller <see cref="AvatarView"/>
    /// </summary>
    public AvatarView avatarView;

    private float _countDown;
    private Text timerText;


    /// <summary>
    /// initialize timer
    /// </summary>
    void Start(){
        _countDown = time;

        timerText = transform.Find("Timer").GetComponent<Text>();

        gameObject.SetActive(false);// disable on start
    }



    /// <summary>
    /// countdown until time is over
    /// then call adjustBones() method
    /// </summary>
    void Update() {

        _countDown -= Time.deltaTime;
        _countDown = Mathf.Max(_countDown, 0.0f);

        {// update text
            timerText.text = _countDown.ToString("0.0");
        }


        if(_countDown <= 0.0001) {
            // time to adjust body
            if(avatarView != null) {
                avatarView.adjustBones();
            }

            // reset timer
            _countDown = time;

            // disable self
            gameObject.SetActive(false);
        }
    }
}
