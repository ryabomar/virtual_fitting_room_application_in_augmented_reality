﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// UI element to show kinect infrared camera image
/// </summary>
[RequireComponent(typeof(RawImage))]
public class InfraredCamera : MonoBehaviour
{
    /// <summary>GameObject with KinectDataSource script attached <see cref="KinectDataSource"/></summary>
    [Tooltip("GameObject with attached KinectDataSource script")]
    public GameObject _Kinect;
    KinectDataSouce kinectDataSource;

    RawImage colorImage;

    /// <summary>
    /// set up sexture for ui element
    /// </summary>
    void Start() {
        { // check kinect
            if(_Kinect == null) throw new Exception("no kinect body source provided");

            kinectDataSource = _Kinect.GetComponent<KinectDataSouce>();
            if(kinectDataSource == null) throw new Exception("given gameobject is not a kinect body source");
        }

        colorImage = GetComponent<RawImage>();
        colorImage.texture = kinectDataSource.infraredTexture;
    }
}
