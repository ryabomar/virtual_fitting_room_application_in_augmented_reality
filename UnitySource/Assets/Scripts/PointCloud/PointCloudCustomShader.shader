﻿Shader "Unlit/PiontCloudShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _positionsTex ("Positions baked in Texture", 2D) = "white" {}
        _SquareSize ("Square size", float) = 0.01

        _width ("width" , float) = 0 // not in use (for now)
        _height("height", float) = 0 // not in use (for now)
    }
    SubShader 
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            Lighting Off

            CGPROGRAM
            #pragma vertex   vert
            #pragma fragment frag
            #pragma geometry geom

            #include "UnityCG.cginc"


            struct Input_data
            {
                float4 pos : POSITION;
                float2 uv : TEXCOORD0;
                uint vertexId   : SV_VertexID;
            };

            struct VertToGeom_data 
            {
                float2 uv  : TEXCOORD0;
                float4 pos : SV_POSITION;
            }; 


            struct GeomToFrag_data
            {
                float2 uv  : TEXCOORD0;
                float4 pos : SV_POSITION;
            }; 

            // ----------------------- UNIFORMS ---v
            sampler2D _MainTex;
            sampler2D _positionsTex; 
            float4 _MainTex_ST;

            float _width;
            float _height;

            uniform float _SquareSize;
            // ----------------------- !UNIFORMS ---^


            // -------------------------------------------- VERTEX SHADER ----v
            VertToGeom_data vert (Input_data data)
            {
                // bypass data to the next stage 
                VertToGeom_data out_data;
                out_data.pos = data.pos;
                out_data.uv = data.uv;
                return out_data;
            }
            // ------------------------------------------- !VERTEX SHADER ----^


            // -------------------------------------------- GEOMETRY SHADER ----v
            /** for each vertex creates two triangles */
            [maxvertexcount(6)] 
            void geom(point VertToGeom_data input[1], inout TriangleStream<GeomToFrag_data> triangleStream) {
                
                GeomToFrag_data topLeft;
                GeomToFrag_data topRight;
                GeomToFrag_data botLeft;
                GeomToFrag_data botRight;


                float4 pos = input[0].pos;
                float squareSize = _SquareSize / 2.0f;


                // offset in camera space // square will always face camera
                topLeft.pos  = mul(UNITY_MATRIX_MV, float4(pos.xyz, 1.0)) + float4(-squareSize,  squareSize, 0, 0);
                topRight.pos = mul(UNITY_MATRIX_MV, float4(pos.xyz, 1.0)) + float4( squareSize,  squareSize, 0, 0);
                botLeft.pos  = mul(UNITY_MATRIX_MV, float4(pos.xyz, 1.0)) + float4(-squareSize, -squareSize, 0, 0);
                botRight.pos = mul(UNITY_MATRIX_MV, float4(pos.xyz, 1.0)) + float4( squareSize, -squareSize, 0, 0);

                topLeft.pos  = mul(UNITY_MATRIX_P, float4(topLeft.pos .xyz, 1.0));
                topRight.pos = mul(UNITY_MATRIX_P, float4(topRight.pos.xyz, 1.0));
                botLeft.pos  = mul(UNITY_MATRIX_P, float4(botLeft.pos .xyz, 1.0));
                botRight.pos = mul(UNITY_MATRIX_P, float4(botRight.pos.xyz, 1.0));

                float2 uv = input[0].uv;
                topLeft.uv  = uv;
                topRight.uv = uv;
                botLeft.uv  = uv;
                botRight.uv = uv;

                // first triangle
                    triangleStream.Append(topLeft);
                    triangleStream.Append(topRight);
                    triangleStream.Append(botLeft);
                triangleStream.RestartStrip();

                // second tiangle
                    triangleStream.Append(botLeft);
                    triangleStream.Append(topRight);
                    triangleStream.Append(botRight);
                triangleStream.RestartStrip();
            }
            // -------------------------------------------- !GEOMETRY SHADER ----^


            // -------------------------------------------- FRAGMENT SHADER ----v
            fixed4 frag (GeomToFrag_data data) : SV_Target
            {
                fixed4 color = tex2D(_MainTex, data.uv);

                return color;
            }
            // ------------------------------------------- !FRAGMENT SHADER ----^
            ENDCG
        }
    }
}
