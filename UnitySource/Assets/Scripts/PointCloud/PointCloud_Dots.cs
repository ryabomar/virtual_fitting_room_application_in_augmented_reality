﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Windows.Kinect;

/// <summary>
/// makes point cloud from mesh vertices
/// </summary>
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class PointCloud_Dots : MonoBehaviour
{
    /// <summary>GameObject with KinectDataSource script attached <see cref="KinectDataSource"/></summary>
    [Tooltip("GameObject with attached KinectDataSource script")]
    public GameObject _Kinect;
    KinectDataSouce kinectDataSouce;

    /// <summary>
    /// creates mesh of (depthData size) vertices
    /// </summary>
    void Start() {
        { // check kinect
            if(_Kinect == null) throw new Exception("no kinect body source provided");

            kinectDataSouce = _Kinect.GetComponent<KinectDataSouce>();
            if(kinectDataSouce == null) throw new Exception("given gameobject is not a kinect body source");
        }

        { // mesh and material
            uint nVertices = kinectDataSouce.depthFrameDesc.LengthInPixels;
            GetComponent<MeshFilter>().mesh = makeMeshOfDisconnectedVertices(nVertices);

            MeshRenderer renderer = GetComponent<MeshRenderer>();
            renderer.material.mainTexture = kinectDataSouce.colorTexture;
        }
    }



    /// <summary>
    /// calculates position and uv fore each vertex
    /// </summary>
    void Update() {
        ushort[] depthData = kinectDataSouce.depthData;

        ColorSpacePoint[] colorSpacePoints = new ColorSpacePoint[depthData.Length]; // UV
        kinectDataSouce.coordinateMapper.MapDepthFrameToColorSpace(depthData, colorSpacePoints);
        //



        CameraSpacePoint[] cameraSpacePoints = new CameraSpacePoint[depthData.Length];
        kinectDataSouce.coordinateMapper.MapDepthFrameToCameraSpace(depthData, cameraSpacePoints);

        {// update positions
            Mesh mesh = GetComponent<MeshFilter>().mesh;
            Vector3[] vertices = new Vector3[depthData.Length];
            Vector2[] uv       = new Vector2[depthData.Length];


            int colorFrameWidth = kinectDataSouce.colorFrameDesc.Width;
            int colorFrameHeight = kinectDataSouce.colorFrameDesc.Height;

            // assume depthData.Length == nVertices
            for(int i = 0; i < depthData.Length; i++) {
                vertices[i].x = cameraSpacePoints[i].X;
                vertices[i].y = cameraSpacePoints[i].Y;
                vertices[i].z = cameraSpacePoints[i].Z;

                uv[i].x = colorSpacePoints[i].X;
                uv[i].y = colorSpacePoints[i].Y;

                uv[i].x = colorSpacePoints[i].X / colorFrameWidth;
                uv[i].y = colorSpacePoints[i].Y / colorFrameHeight;
            }

            mesh.vertices = vertices;
            mesh.uv = uv;
        }
        
    }


    /// <summary>
    /// create mesh of vertices without faces
    /// </summary>
    /// <param name="nVertices">number of vertices</param>
    /// <returns>new mesh</returns>
    static private Mesh makeMeshOfDisconnectedVertices(uint nVertices) {
        // create new mesh
        Mesh mesh = new Mesh();

        if(nVertices >= 65535) {
            // to make mesh of more then 65535 vertices
            mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32; 
        }

        Vector3[] vertices = new Vector3[nVertices];
        Vector2[] uv       = new Vector2[nVertices];
        int[]     indices  = new int    [nVertices];

        for(int i =0; i < nVertices; ++i) {
            indices [i] = i;
            vertices[i] = Vector3.zero;
            uv      [i] = Vector2.zero;
        }

        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.SetIndices(indices, MeshTopology.Points, 0);

        return mesh;
    }
}
