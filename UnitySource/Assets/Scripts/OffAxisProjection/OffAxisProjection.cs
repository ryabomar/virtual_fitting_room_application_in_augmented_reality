﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// calculate off-axis matrix and applies it on camera
/// </summary>
[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class OffAxisProjection : MonoBehaviour
{
    /// <summary>
    /// projection frame <see cref="ProjectionPlane"/>
    /// </summary>
    public ProjectionPlane projectionPlane;

    /// <summary>
    /// to draw lines or not
    /// </summary>
    public bool DrawGizmo = true;

    private Vector3 eyePosition;
    //From eye to projection screen corners
    private float n, f;

    private Vector3 va, vb, vc, vd;

    //Extents of perpendicular projection
    float l, r, b, t;

    private Vector3 viewDirection;

    private Camera cam;

    private void Awake()
    {
        cam = GetComponent<Camera>();

    }


    /// <summary>
    /// draw lines for debug purpose
    /// </summary>
    private void OnDrawGizmos()
    {
        if (projectionPlane == null)
            return;

        if (DrawGizmo)
        {
            var pos = transform.position;
            Gizmos.color = Color.green;
            Gizmos.DrawLine(pos, pos + va);
            Gizmos.DrawLine(pos, pos + vb);
            Gizmos.DrawLine(pos, pos + vc);
            Gizmos.DrawLine(pos, pos + vd);

            Gizmos.color = Color.white;
            Gizmos.DrawLine(pos, viewDirection);
        }
    }


    /// <summary>
    /// calculate matrices and apply them to camera
    /// </summary>
    private void LateUpdate()
    {
        if (projectionPlane != null)
        {
            Vector3 pa = projectionPlane.pa;
            Vector3 pb = projectionPlane.pb;
            Vector3 pc = projectionPlane.pc;
            Vector3 pd = projectionPlane.pd;

            Vector3 vr = projectionPlane.vr;
            Vector3 vu = projectionPlane.vu;
            Vector3 vn = projectionPlane.vn;

            Matrix4x4 M = projectionPlane.M;

            eyePosition = transform.position;

            //From eye to projection screen corners
            va = pa - eyePosition;
            vb = pb - eyePosition;
            vc = pc - eyePosition;
            vd = pd - eyePosition;

            viewDirection = eyePosition + va + vb + vc + vd;

            //distance from eye to projection screen plane
            float d = -Vector3.Dot(va, vn);

            cam.nearClipPlane = d;
            n = cam.nearClipPlane;
            f = cam.farClipPlane;

            float nearOverDist = n / d;
            l = Vector3.Dot(vr, va) * nearOverDist;
            r = Vector3.Dot(vr, vb) * nearOverDist;
            b = Vector3.Dot(vu, va) * nearOverDist;
            t = Vector3.Dot(vu, vc) * nearOverDist;
            Matrix4x4 P = Matrix4x4.Frustum(l, r, b, t, n, f);

            //Translation to eye position
            Matrix4x4 T = Matrix4x4.Translate(-eyePosition);


            Matrix4x4 R = Matrix4x4.Rotate(Quaternion.Inverse(transform.rotation) * projectionPlane.transform.rotation);
            cam.worldToCameraMatrix = M * R * T;

            cam.projectionMatrix = P;
        }
    }
}
