﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// works in pair with <c>OffAxisProjection</c>
/// represents frame calera looks through
/// </summary>
[ExecuteInEditMode]
public class ProjectionPlane : MonoBehaviour
{
    /// <summary>
    /// to draw lines or not
    /// </summary>
    public bool DrawGizmo = true;

    /// <summary>
    /// if enabled will keep proportion of frame the same as application window
    /// </summary>
    public bool keepScreenProportion = false;


    // projection plane corners
    public Vector3 pa { get; private set; }
    public Vector3 pb { get; private set; }
    public Vector3 pc { get; private set; }
    public Vector3 pd { get; private set; }

    // projection plane basis vectors
    public Vector3 vu { get; private set; }
    public Vector3 vr { get; private set; }  
    public Vector3 vn { get; private set; }

    Matrix4x4 m;
    public Matrix4x4 M { get => m; }

    /// <summary>
    /// draw lines for debug purpose
    /// </summary>
    void OnDrawGizmos()
    {
        if (DrawGizmo)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(pa, pb);
            Gizmos.DrawLine(pa, pc);
            Gizmos.DrawLine(pd, pb);
            Gizmos.DrawLine(pc, pd);
        }
    }


    // recalculate matrix each frame
    void Update()
    {
        if(keepScreenProportion) {
            float ratio = (float)Screen.width / Screen.height;
            transform.localScale = new Vector3(ratio * transform.localScale.y, transform.localScale.y, transform.localScale.z);
        }

        pa = transform.TransformPoint(new Vector3(-0.5f, -0.5f, 0.0f));
        pb = transform.TransformPoint(new Vector3( 0.5f, -0.5f, 0.0f));
        pc = transform.TransformPoint(new Vector3(-0.5f,  0.5f, 0.0f));
        pd = transform.TransformPoint(new Vector3( 0.5f,  0.5f, 0.0f));


        vr = (pb - pa).normalized;
        vu = (pc - pa).normalized;
        vn = -Vector3.Cross(vr, vu).normalized;

        m = Matrix4x4.zero;
        m[0, 0] = vr.x;
        m[0, 1] = vr.y;
        m[0, 2] = vr.z;

        m[1, 0] = vu.x;
        m[1, 1] = vu.y;
        m[1, 2] = vu.z;

        m[2, 0] = vn.x;
        m[2, 1] = vn.y;
        m[2, 2] = vn.z;

        m[3, 3] = 1.0f;
    }
}
