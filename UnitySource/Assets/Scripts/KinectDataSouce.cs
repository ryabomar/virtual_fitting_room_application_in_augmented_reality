﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Windows.Kinect;

/// <summary>
/// Class <c>KinectDataSouce</c>
/// incorporates Kinect SDK interface into single class with properties
/// </summary>
public class KinectDataSouce : MonoBehaviour
{
    private KinectSensor sensor;
    private ColorFrameReader    colorReader;
    private DepthFrameReader    depthReader;
    private InfraredFrameReader infraredReader;
    private BodyFrameReader     bodyReader;

    /// <summary>description of color frame <see cref="Windows.Kinect.FrameDescription"/></summary>
    public FrameDescription    colorFrameDesc    { get; private set; }

    /// <summary>description of infrared frame <see cref="Windows.Kinect.FrameDescription"/></summary>
    public FrameDescription    infraredFrameDesc { get; private set; }

    /// <summary>description of depth frame <see cref="Windows.Kinect.FrameDescription"/></summary>
    public FrameDescription    depthFrameDesc    { get; private set; }


    /// <summary>color data for last frame</summary>
    public byte[] colorData { get; private set; }

    ///<summary>color camera as texture (updates continually)</summary>
    public Texture2D colorTexture { get; private set; }



    /// <summary>infrared intensivity data for last frame</summary>
    public ushort[]  infraredData  { get; private set; }

    private byte[]    infraredRawData; //internal use

    ///<summary>infrared camera as texture (updates continually)</summary>
    public Texture2D infraredTexture { get; private set; }



    /// <summary>depth data for last frame</summary>
    public ushort[]  depthData { get; private set; }

    /// <summary> minimal reliable depth</summary>
    public ushort depthMin {  get; private set; }

    /// <summary> maximum reliable depth</summary>
    public ushort depthMax { get; private set; }


    private byte[] depthData_buffer;//internal use

    /// <summary> last frame of bodies kinect knows</summary>
    public Body[]    bodyData { get; private set; }

    /// <summary> coordinate mapper <see cref="Windows.Kinect.CoordinateMapper"/></summary>
    public CoordinateMapper coordinateMapper { get; private set; }
    
    /// <summary>kinect sensor initialization</summary>
    void Awake() {
        sensor = KinectSensor.GetDefault();
        if(sensor == null) throw new Exception("Could not get kinect sensor");

        {// init color reader
            colorReader = sensor.ColorFrameSource.OpenReader();
            colorFrameDesc = sensor.ColorFrameSource.CreateFrameDescription(ColorImageFormat.Rgba);
            colorTexture = new Texture2D(colorFrameDesc.Width, colorFrameDesc.Height, TextureFormat.RGBA32, false); // без mipmap
            colorData = new byte[colorFrameDesc.BytesPerPixel * colorFrameDesc.LengthInPixels];
        }

        {// infrared
            infraredReader = sensor.InfraredFrameSource.OpenReader();
            infraredFrameDesc = sensor.InfraredFrameSource.FrameDescription;
            infraredData = new ushort[infraredFrameDesc.LengthInPixels];
            infraredRawData = new byte[infraredFrameDesc.LengthInPixels * 4];
            infraredTexture = new Texture2D(infraredFrameDesc.Width, infraredFrameDesc.Height, TextureFormat.BGRA32, false);
        }

        {// init depth reader
            depthReader = sensor.DepthFrameSource.OpenReader();
            depthFrameDesc = sensor.DepthFrameSource.FrameDescription;
            depthData = new ushort[depthFrameDesc.LengthInPixels];
            depthMin = sensor.DepthFrameSource.DepthMinReliableDistance;
            depthMax = sensor.DepthFrameSource.DepthMaxReliableDistance;

            depthData_buffer = new byte[depthData.Length * 2];
        }

        {// init body reader
            bodyReader = sensor.BodyFrameSource.OpenReader();
            bodyData = new Body[sensor.BodyFrameSource.BodyCount];
        }

        coordinateMapper = sensor.CoordinateMapper;
        

        if(!sensor.IsOpen) sensor.Open();
    }

    /// <summary>each frame resfeshes data from sensor</summary>
    void Update() {

        if(sensor == null) return; // kinect not initialized

        // update color data
        if(colorReader != null) {
            ColorFrame frame = colorReader.AcquireLatestFrame();
            if(frame != null) {
                frame.CopyConvertedFrameDataToArray(colorData, ColorImageFormat.Rgba);
                colorTexture.LoadRawTextureData(colorData);
                colorTexture.Apply();
                frame.Dispose();
            }
        }

        {// update infrared
            InfraredFrame frame = infraredReader.AcquireLatestFrame();
            if(frame != null) {
                frame.CopyFrameDataToArray(infraredData);
                int i = 0;
                foreach(var ir in infraredData) {
                    byte intensity = (byte)(ir >> 8);
                    infraredRawData[i++] = intensity;
                    infraredRawData[i++] = intensity;
                    infraredRawData[i++] = intensity;
                    infraredRawData[i++] = 255;
                }
                infraredTexture.LoadRawTextureData(infraredRawData);
                infraredTexture.Apply();

                frame.Dispose();
            }
        }

        // update depth data
        if(depthReader != null) {
            DepthFrame depthFrame = depthReader.AcquireLatestFrame();

            if(depthFrame != null) {
                depthFrame.CopyFrameDataToArray(depthData);
                Buffer.BlockCopy(depthData, 0, depthData_buffer, 0, depthData.Length * 2);
                depthFrame.Dispose();
            }
        }

        // update body data
        if(bodyReader != null) {
            BodyFrame frame = bodyReader.AcquireLatestFrame();
            if(frame != null) {
                frame.GetAndRefreshBodyData(bodyData);
                frame.Dispose();
            }
        }


    }


    /// <summary> reinitialize kinect sensor on quit</summary>
    void OnApplicationQuit() {
        if(colorReader != null) {
            colorReader.Dispose();
            colorReader = null;
        }

        if(depthReader != null) {
            depthReader.Dispose();
            depthReader = null;
        }

        if(bodyReader != null) {
            bodyReader.Dispose();
            bodyReader = null;
        }

        if(sensor != null) {
            if(sensor.IsOpen) sensor.Close();
            sensor = null;
        }
    }
}
