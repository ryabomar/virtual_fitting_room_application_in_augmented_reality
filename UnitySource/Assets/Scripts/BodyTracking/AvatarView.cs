﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Windows.Kinect;


/// <summary> Class <c>AvatarView</c>
/// an avatar controller;
/// do instantiates prefab and applies IK motion on it
/// </summary>
public class AvatarView : MonoBehaviour
{

    [Header("Adjust avatar")]
    public Vector3 avatarScale    = new Vector3(1,1,1);
    public Vector3 hipOffset      = new Vector3(0, 0, 0);
    public Vector3 HipsTuneEuler  = new Vector3(0, 0, 0);
    public Vector3 SpineTuneEuler = new Vector3(0, 0, 0);
    public Vector3 headOffset     = new Vector3(0, 0, 0);

    /// <summary>flip left/right side</summary>
    public bool mirrored = true;

    private Vector3 initAvatarScale;

    [Space(10)]
    [Tooltip("prefab of human like avatar")]
    /// <summary>default avatar prefab</summary>
    public GameObject avatarPrefab;

    [Tooltip("GameObject with attached KinectDataSource script")]
    /// <summary>GameObject with KinectDataSource script attached <see cref="KinectDataSource"/></summary>
    public GameObject _Kinect;
    KinectDataSouce kinectDataSouce;


    Body trackingBody = null;


    GameObject avatarObj; // instance of avatar
    Animator animator; // animator component from avatar


    /// <summary>
    /// inverse kinematic chains for different limbs; <see cref="IKChain"/>
    /// </summary>

    IKChain leftHand, rightHand, leftLeg, rightLeg, spineChain, headChain;

    //Quaternion hipsInitRotation, spineInitRotation;
    Dictionary<HumanBodyBones, Quaternion> initRotations = new Dictionary<HumanBodyBones, Quaternion>();


    void Start() {
        { // check kinect
            if(_Kinect == null) throw new ArgumentNullException("no kinect body source provided");

            kinectDataSouce = _Kinect.GetComponent<KinectDataSouce>();
            if(kinectDataSouce == null) throw new ArgumentException("given gameobject is not a kinect body source");
        }

        changeAvatar(avatarPrefab); // set default avatar

        initAvatarScale = avatarScale;
    }




    /// <summary>
    /// instatniate prefab as avatar; destroes old avatar if present
    /// </summary>
    /// <param name="newAvatar">prefab to instantiate</param>
    public void changeAvatar(GameObject newAvatar) {
        GameObject obj;
        {//  instantiate avatar prefab
            obj = Instantiate(newAvatar);
            if(obj.GetComponent<Animator>() == null 
            || !obj.GetComponent<Animator>().avatar.isHuman 
            || !obj.GetComponent<Animator>().avatar.isValid) 
            {

                throw new ArgumentException("prefab should contain Animator component and be valid human avatar");
            }
        }


        // destroy previous avatar
        if(avatarObj != null) { 

            leftHand = null;
            rightHand = null;
            leftLeg = null;
            rightLeg = null;
            headChain = null;
            spineChain = null;
            initRotations.Clear();

            GameObject.Destroy(avatarObj);
            avatarObj = null;
        }

        {

            avatarObj = obj;
            avatarObj.transform.parent = transform;
            avatarObj.transform.localEulerAngles = new Vector3(0, 180, 0);
            avatarObj.transform.localPosition = Vector3.zero;
            avatarObj.transform.localScale = avatarScale;

            animator = avatarObj.GetComponent<Animator>();

            // disable mecanim animation
            animator.enabled = false;
        }

        //Debug.Log(animator.GetBoneTransform(HumanBodyBones.Hips).rotation.eulerAngles);
        initRotations[HumanBodyBones.Hips] = animator.GetBoneTransform(HumanBodyBones.Hips).rotation;
        initRotations[HumanBodyBones.Spine] = animator.GetBoneTransform(HumanBodyBones.Spine).rotation;
        initRotations[HumanBodyBones.Neck] = animator.GetBoneTransform(HumanBodyBones.Neck).rotation;
        initRotations[HumanBodyBones.Head] = animator.GetBoneTransform(HumanBodyBones.Head).rotation;

        {// make IK chains for hands and legs
            makeIkChains();
        }
    }

    /// <summary>
    /// update body data
    /// </summary>
    void Update() {
        if(kinectDataSouce == null) return;

        foreach(Body body in kinectDataSouce.bodyData) {
            if(body == null)    { continue; }
            if(body.IsTracked)  { trackingBody = body; return; }
        }

        trackingBody = null;
    }


    /// <summary>
    /// apply body data on avatar bones
    /// </summary>
    void LateUpdate() {
        if(avatarObj == null || trackingBody == null) return;

        avatarObj.transform.localScale = avatarScale;

        // work only if animator is disabled
        if(animator.enabled) return;


        { // hips
            Transform hips = animator.GetBoneTransform(HumanBodyBones.Hips);

            // position
            Vector3 position = utils.jointPosition(trackingBody.Joints[JointType.SpineBase]);
            hips.position = position + hipOffset;

            // orientation
            Vector3 center = localJointPosition(trackingBody.Joints[JointType.SpineMid]);
            Vector3 leftDir = center - localJointPosition(trackingBody.Joints[JointType.HipLeft]);
            Vector3 rightDir = center - localJointPosition(trackingBody.Joints[JointType.HipRight]);
            Vector3 upDir = center - localJointPosition(trackingBody.Joints[JointType.SpineBase]);

            Vector3 target = mirrored ? Vector3.Cross(rightDir, leftDir) : Vector3.Cross(leftDir, rightDir);

            hips.rotation =
                Quaternion.LookRotation(target, upDir)
                * Quaternion.Euler(HipsTuneEuler)
                * initRotations[HumanBodyBones.Hips];
        }

        { // head and spine
            //Vector3 extenderHeadJointPos = 

            //headChain.refresh(utils.jointPosition(trackingBody.Joints[JointType.Head]) + headOffset,
            //    new Vector3[]{
            //        utils.jointPosition(trackingBody.Joints[JointType.Neck]),
            //        //utils.jointPosition(trackingBody.Joints[JointType.SpineShoulder]),
            //        //utils.jointPosition(trackingBody.Joints[JointType.SpineMid]),
            //    });

            // spine
            //spineChain.refresh(utils.jointPosition(trackingBody.Joints[JointType.SpineShoulder]),
            //   new Vector3[]{
            //        utils.jointPosition(trackingBody.Joints[JointType.SpineMid]),
            //   });


            // rotate ribcage
            //Transform ribcage = animator.GetBoneTransform(HumanBodyBones.Spine);
            //Vector3 ribcageNormal = utils.makeNormal(
            //        localJointPosition(trackingBody.Joints[JointType.SpineMid]),
            //        localJointPosition(trackingBody.Joints[JointType.ShoulderLeft]),
            //        localJointPosition(trackingBody.Joints[JointType.ShoulderRight])
            //    );

            //if(mirrored) { ribcageNormal *= -1; }

            { // rotate ribcage
                Transform spine = animator.GetBoneTransform(HumanBodyBones.Spine);
                // rotation
                Vector3 center = localJointPosition(trackingBody.Joints[JointType.SpineMid]);
                Vector3 leftDir = center - localJointPosition(trackingBody.Joints[JointType.ShoulderLeft]);
                Vector3 rightDir = center - localJointPosition(trackingBody.Joints[JointType.ShoulderRight]);
                Vector3 upDir = center - localJointPosition(trackingBody.Joints[JointType.SpineShoulder]);

                Vector3 target = mirrored ? Vector3.Cross(leftDir, rightDir) : Vector3.Cross(rightDir, leftDir);

                spine.rotation =
                    Quaternion.LookRotation(target, -upDir)
                    * Quaternion.Euler(SpineTuneEuler)
                    * initRotations[HumanBodyBones.Spine];
            }

        }

        




        {// hands
            IKChain hand_1 = !mirrored ? rightHand : leftHand;
            IKChain hand_2 = !mirrored ? leftHand : rightHand;

            hand_1.refresh(utils.jointPosition(trackingBody.Joints[JointType.HandLeft]),
                new Vector3[] {
                    utils.jointPosition(trackingBody.Joints[JointType.ElbowLeft]),
                });

            hand_2.refresh(utils.jointPosition(trackingBody.Joints[JointType.HandRight]),
                 new Vector3[] {
                    utils.jointPosition(trackingBody.Joints[JointType.ElbowRight]),
                 });
        }


        {// legs
            IKChain leg_1 = !mirrored ? rightLeg : leftLeg;
            IKChain leg_2 = !mirrored ? leftLeg : rightLeg;

            leg_1.refresh(utils.jointPosition(trackingBody.Joints[JointType.AnkleLeft]),
                utils.jointPosition(trackingBody.Joints[JointType.KneeLeft])
            );

            leg_2.refresh(utils.jointPosition(trackingBody.Joints[JointType.AnkleRight]),
                utils.jointPosition(trackingBody.Joints[JointType.KneeRight])
            );
        }
    }



    /// <summary>
    /// creates inverse kinematic chains for limbs
    /// </summary>
    private void makeIkChains() {
        leftHand  = new IKChain(animator.GetBoneTransform(HumanBodyBones.LeftHand), 2);
        rightHand = new IKChain(animator.GetBoneTransform(HumanBodyBones.RightHand), 2);
        leftLeg   = new IKChain(animator.GetBoneTransform(HumanBodyBones.LeftFoot), 2);
        rightLeg  = new IKChain(animator.GetBoneTransform(HumanBodyBones.RightFoot), 2);
        headChain = new IKChain(animator.GetBoneTransform(HumanBodyBones.Head), 1);
        spineChain = new IKChain(animator.GetBoneTransform(HumanBodyBones.Neck), 2);
    }


    ///<summary> 
    /// translate joint position into avatar local space
    /// </summary>
    private Vector3 localJointPosition(Windows.Kinect.Joint joint) {
        Vector3 pos = utils.jointPosition(joint);
        return transform.InverseTransformPoint(pos);
    }

    // ---------------------------------------------------------------------------------------------------------
    /// <summary>
    /// stores joint and joint upper in hyerarhy <example><c>new JointPair(JointType.Neck, JointType.SpineShoulder)</c></example>
    /// for internal use
    /// </summary>
    struct JointPair {
        public JointType joint, parentJoint;
        public JointPair(JointType joint, JointType parentJoint) { this.joint = joint; this.parentJoint = parentJoint; }
    };

    
    /// <summary>
    /// changes avatar bones lengths to the size of distance between corresponding joints
    /// </summary>
    public void adjustBones() {
        if(animator == null) throw new Exception("no animator component");

        Dictionary<HumanBodyBones, JointPair> bonesToFit = new Dictionary<HumanBodyBones, JointPair>() {
            //
            // bone                               // joint (copy its local position)  | parent joint (to calculate local position)
            // spine
            //{ HumanBodyBones.Hips,  new JointPair(JointType.SpineBase,     JointType.SpineMid) },
            //{ HumanBodyBones.Spine, new JointPair(JointType.SpineMid, JointType.SpineBase) },
            //{ HumanBodyBones.Neck,  new JointPair(JointType.SpineShoulder, JointType.SpineMid) },
            { HumanBodyBones.Head,          new JointPair(JointType.Neck, JointType.SpineShoulder) },

            //left leg
            { HumanBodyBones.LeftUpperLeg,  new JointPair(JointType.HipLeft,   JointType.SpineBase)},
            { HumanBodyBones.LeftLowerLeg,  new JointPair(JointType.KneeLeft,  JointType.HipLeft)},
            { HumanBodyBones.LeftFoot,      new JointPair(JointType.AnkleLeft, JointType.KneeLeft)},

            //right leg
            { HumanBodyBones.RightUpperLeg, new JointPair(JointType.HipRight,   JointType.SpineBase)},
            { HumanBodyBones.RightLowerLeg, new JointPair(JointType.KneeRight,  JointType.HipRight)},
            { HumanBodyBones.RightFoot,     new JointPair(JointType.AnkleRight, JointType.KneeRight)},

            //left arm
            { HumanBodyBones.LeftLowerArm,  new JointPair(JointType.ElbowLeft,   JointType.ShoulderLeft)},
            { HumanBodyBones.LeftHand,      new JointPair(JointType.WristLeft,   JointType.ElbowLeft)},

            //right arm
            { HumanBodyBones.RightLowerArm,  new JointPair(JointType.ElbowRight, JointType.ShoulderRight)},
            { HumanBodyBones.RightHand,      new JointPair(JointType.WristRight, JointType.ElbowRight)},
        };

        foreach(var entry in bonesToFit) {
            HumanBodyBones boneType   = entry.Key;
            JointType jointType       = entry.Value.joint;
            JointType parentJointType = entry.Value.parentJoint;

            // find bone in humanoid armature
            Transform bone = animator.GetBoneTransform(boneType);
            if(bone == null) continue; // no such bone 

            // calc bone length
            Vector3 delta = utils.jointPosition(trackingBody.Joints[jointType]) - utils.jointPosition(trackingBody.Joints[parentJointType]);

            Transform rootTransform = animator.GetBoneTransform(HumanBodyBones.Hips);


            float length = rootTransform.InverseTransformVector(delta).magnitude;

            // adjust new length
            bone.localPosition = bone.localPosition.normalized * Math.Abs(length);
        }

        // recreate IK chains 
        // (IKChain stores bones lengths then constructed so its necessary to recreate them. Oribably should be rewritten)
        makeIkChains();
    }


    /// <summary>
    /// moves root avatar bone (hips) in Y axis by value
    /// </summary>
    /// <param name="value">offset to apply</param>
    public void changeHipYOffset(float value) {
        hipOffset.y = value - 0.5f;
    }


    /// <summary>
    /// changes scale of avatar object by X Y and Z axis simultaneously
    /// </summary>
    /// <param name="value">delta of scale</param>
    public void changeAvatarScale(float value) {
        //public Vector3 avatarScale = new Vector3(1, 1, 1);
        value = value - 0.5f;
        avatarScale = initAvatarScale + new Vector3(value, value, value);
    }
}
