﻿using UnityEngine;
using System;

/// <summary>
/// chain of objects for IK algorithm application
/// </summary>
public class IKChain {

    /// <summary>how many nodes in the chain</summary>
    public int length { get => nLinks - 1; }

    /// <summary>how many shoulders in chain</summary>
    public int nLinks { get; private set; }

    protected internal Transform[] links;
    protected internal float[] linkLengths;


    /// <summary>maximum length of chain if it is straight</summary>
    protected float fullLength = 0.0f;


    /// <summary>root object</summary>
    protected Transform root { get => links[length - 1]; }


    /// <summary>
    /// constructs chain based on existing GameObjects
    /// </summary>
    /// <param name="effector">object on the end of the chain <example>foot for leg chain</example></param>
    /// <param name="length">how many objects will be in chain</param>
    public IKChain(Transform effector, int length) {
        { // init chain
            nLinks = length + 1;
            links = new Transform[nLinks];
            linkLengths = new float[nLinks];

            links[0] = effector;
            linkLengths[0] = 0.0f;

            for(int i = 1; i < nLinks; ++i) {
                if(links[i - 1].parent == null)
                    throw new Exception("transform parent-child chain is not long enough to make IKChain desirable length [actual:" + (i - 1) + "dezire:" + length + "]");

                links[i] = links[i - 1].parent;
                linkLengths[i] = (links[i].position - links[i - 1].position).magnitude;
                fullLength += linkLengths[i];
            }
        }


        { // init fabric
            positions = new Vector3[nLinks];
            initRotations = new Quaternion[nLinks];
            initDirections = new Vector3[nLinks];

            for(int i = 0; i < nLinks; ++i) {
                initRotations[i] = links[i].rotation;
            }


            for(int i = 1; i < nLinks; ++i) {
                initDirections[i] = links[i - 1].position - links[i].position;
            }
        }
    }


    /// <summary>
    /// updates positions of objects in chain using IK algorithm
    /// <example>
    /// <code>
    /// Vector3 targetPos = new Vector3(10,0,1); // target for foot
    /// Vector3 kneePos = new Vector3(10,0,1);
    /// Vector3 anklePos = new Vector3(10,0,1);
    /// 
    /// IKChain leg = new IKChain(...);
    /// leg.refresh(targetPos, new Vector3[]{kneePos, anklePos});
    /// </code>
    /// </example>
    /// </summary>
    /// <param name="target">position to try to reach</param>
    /// <param name="hints">positions for middle objects to aim;</param>
    public void refresh(Vector3 target, Vector3[] hints) {
        float sqrDistance = (target - root.position).sqrMagnitude;

        // check if target is reachable
        if(sqrDistance >= (fullLength * fullLength)) {
            // unreachable
            stretchTo(target);
        } else {
            // use algorithm
            FABRIK(target, hints, 16, 0.001f);
        }
    }


    /// <summary>
    /// polymorphic overload in case of no hints
    /// </summary>
    /// <param name="target">position to try to reach</param>
    public virtual void refresh(Vector3 target) {
        refresh(target, new Vector3[0]);
    }


    /// <summary>
    /// polymorphic overload in case of only one hint
    /// </summary>
    /// <param name="target">position to try to reach</param>
    public virtual void refresh(Vector3 target, Vector3 hint) {
        refresh(target, new Vector3[] { hint });
    }




    // ----- fabric variables ---v
    private Vector3[] positions;
    private Quaternion[] initRotations;
    private Vector3[] initDirections;
    // --------------------------^

    /// <summary>
    /// Forward And Backward Reaching Inverse Kinematics algorithm
    /// </summary>
    /// <param name="target">position to try to reach</param>
    /// <param name="hints">positions for middle objects to aim</param>
    /// <param name="maxIterations">threshold value</param>
    /// <param name="precision">how close effector must be to the target to satisfy the conditions</param>
    /// <returns>number of iteration</returns>
    private int FABRIK(Vector3 target, Vector3[] hints, int maxIterations, float precision) {
        int n = 1;
        for(; n <= maxIterations; ++n) {

            { // ---------------------------------------------------------------- forward ----v
              //move affector to the target
                positions[0] = target;

                int i = 1;
                for(; n == 1 && i < Math.Min(hints.Length + 1, nLinks); ++i) {
                    // apply hints on first iteration
                    positions[i] = hints[i - 1];
                }


                for(; i < nLinks; ++i) {
                    Vector3 d = positions[i - 1] - positions[i];
                    positions[i] = positions[i - 1] - d.normalized * linkLengths[i];
                }
            } // --------------------------------------------------------------- !forward ----^

            { // --------------------------------------------------------------- backward ----v
              // move root to its origin
                positions[nLinks - 1] = links[nLinks - 1].position;

                for(int i = nLinks - 1; i > 0; --i) {
                    Vector3 d = positions[i - 1] - positions[i];
                    positions[i - 1] = positions[i] + d.normalized * linkLengths[i];
                }
            } // -------------------------------------------------------------- !backward ----^

            { // --------------------------------------- check if solution is good enough ----
                if((positions[0] - target).sqrMagnitude < (precision * precision)) {
                    break;
                }
            }

        } // !for

        // apply solution on bones
        for(int i = nLinks - 1; i > 0; --i) {

            Vector3 newBoneDir = positions[i - 1] - positions[i];

            links[i].rotation =
                Quaternion.FromToRotation(initDirections[i], newBoneDir)
                * initRotations[i];
        }

        return n;
    }

    // used if target is unreachable
    /// <summary>
    /// straightens the chain towards target
    /// </summary>
    /// <param name="target">position to aim</param>
    private void stretchTo(Vector3 target) {
        for(int i = nLinks - 1; i >= 1; --i) {
            Vector3 boneDir = links[i - 1].position - links[i].position;
            Vector3 targetDir = target - links[i].position;
            links[i].rotation = Quaternion.FromToRotation(boneDir, targetDir) * links[i].rotation;
        }
    }

}
