﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Windows.Kinect;

/// <summary>
/// Draws wireframe humanoid for debug purpose
/// </summary>
public class WireframeBodyView : MonoBehaviour
{
    [Tooltip("GameObject with attached KinectDataSource script")]
    public GameObject _Kinect;
    KinectDataSouce kinectDataSource;


    /// <summary>prefab to represent joint</summary>
    public GameObject jointPrefab;

    /// <summary>material for lines between joints</summary>
    public Material jointLineMaterial;


    /// <summary>map joint by type to actual GameObjects</summary>
    Dictionary<JointType, Transform> joints = new Dictionary<JointType, Transform>();


    /// <summary>
    /// instantiate joints
    /// </summary>
    void Start() {
        { // check kinect
            if(_Kinect == null) throw new Exception("no kinect body source provided");

            kinectDataSource = _Kinect.GetComponent<KinectDataSouce>();
            if(kinectDataSource == null) throw new Exception("given gameobject is not a kinect body source");
        }


        if(jointPrefab == null) throw new Exception("set joint prefab");

        // instantiating prefab for each joint
        for(JointType jointType = JointType.SpineBase; jointType <= JointType.ThumbRight; jointType++) {
            GameObject obj = Instantiate(jointPrefab);
            obj.name = jointType.ToString();

            obj.transform.parent = transform;
            obj.transform.localPosition = Vector3.zero;

            if(utils.BONE_MAP.ContainsKey(jointType)) {
                // will drow the line if joint has other joint on the end
                LineRenderer lineRenderer = obj.AddComponent<LineRenderer>();
                lineRenderer.material = jointLineMaterial;
                lineRenderer.positionCount = 2;
                lineRenderer.startWidth = 0.03f;
                lineRenderer.endWidth   = 0.05f;
            }

            joints[jointType] = obj.transform;
        }
    }


    /// <summary>
    /// updates objects position to represent joins movement
    /// </summary>
    void Update() {
        if (kinectDataSource == null) return;

        Body trackingBody = null;

        // use first tracked body
        foreach(Body body in kinectDataSource.bodyData) {
            if (body == null) { continue; }
            if(body.IsTracked) { trackingBody = body; break;}
        }

        if(trackingBody == null) return; // no tracked body for now

        foreach (var entry in joints) {

            JointType jointType = entry.Key;
            Transform joint     = entry.Value;

            joint.localPosition = utils.jointPosition(trackingBody.Joints[jointType]);
            joint.localRotation = utils.jointRotation(trackingBody.JointOrientations[jointType]);


            { // draw line
                // skip if joint is last in hierarchy
                if(!utils.BONE_MAP.ContainsKey(jointType)) continue;

                // draw line between joints
                LineRenderer lineRenderer = joint.GetComponent<LineRenderer>();

                // line color depends of state
                Color color;
                switch(trackingBody.Joints[jointType].TrackingState){
                    case TrackingState.Tracked:     color = Color.green;    break;
                    case TrackingState.Inferred:    color = Color.red;      break;
                    default:                        color = Color.black;    break;
                }
                
                lineRenderer.startColor = color;
                lineRenderer.endColor   = color;

                Vector3 endPos = utils.jointPosition(trackingBody.Joints[utils.BONE_MAP[jointType]]);

                lineRenderer.SetPosition(0, joint.position);
                lineRenderer.SetPosition(1, transform.TransformPoint(endPos));
            }
        }
    }
}
