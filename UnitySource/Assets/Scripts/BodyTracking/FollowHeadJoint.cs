﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Windows.Kinect;

/// <summary>
/// moves its object to the position of head joint, mirrored from the projection frame
/// </summary>
public class FollowHeadJoint : MonoBehaviour {

    [Tooltip("GameObject with attached KinectDataSource script")]
    /// <summary>GameObject with KinectDataSource script attached <see cref="KinectDataSource"/></summary>
    public GameObject _Kinect;
    KinectDataSouce kinectDataSouce;

    /// <summary>projection frame <see cref="ProjectionPlane"/>/// </summary>
    public GameObject _MirrorFrame;

    [SerializeField]
    private bool _followHead = false;

    /// <summary>flag; determinants behavior</summary>
    public bool followHead {
        get => _followHead;
        set { if(value != _followHead) toggleFollow(); }
    }


    private Vector3 savedPosition;
  
    void Awake() {
        savedPosition = transform.position;
    }

    private void Start() {
        { // check kinect
            if(_Kinect == null) throw new Exception("no kinect body source provided");

            kinectDataSouce = _Kinect.GetComponent<KinectDataSouce>();
            if(kinectDataSouce == null) throw new Exception("given gameobject is not a kinect body source");
        }
    }


    /// <summary>
    /// if <c>followHead</c> set set its object position to the position of head joint, mirrored from the projection frame
    /// </summary>
    void Update() {
        if(!_followHead) return;
        if(kinectDataSouce == null || _MirrorFrame == null) return;

        Body trackingBody = null;
        foreach(Body body in kinectDataSouce.bodyData) {
            if(body == null) { continue; }
            if(body.IsTracked) { trackingBody = body; break; }
        }

        if(trackingBody != null) {
            // find head position
            Vector3 headPos = utils.jointPosition(trackingBody.Joints[JointType.Head]);

            Vector3 headPos_MirrorLocal = _MirrorFrame.transform.InverseTransformPoint(headPos);
            headPos_MirrorLocal.z = -headPos_MirrorLocal.z;

            transform.position = _MirrorFrame.transform.TransformPoint(headPos_MirrorLocal);// reflect from miffor frame

        }
    }


    /// <summary>
    /// switch behavior: follow or not
    /// </summary>
    public void toggleFollow() { 
        if(_followHead) stopFollow();
        else beginFollow();
    }


    /// <summary>
    /// turn off follow head joint
    /// </summary>
    public void stopFollow() {
        if(!_followHead) return;

        _followHead = false;
        transform.position = savedPosition; // restore position
    }


    /// <summary>
    /// turn on follow head joint
    /// </summary>
    public void beginFollow() { 
        if(_followHead) return; // already following

        _followHead = true;
        savedPosition = transform.position; // save position

    }
}
