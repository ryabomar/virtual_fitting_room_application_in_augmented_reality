﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>Class <c>WheelSelector</c> 
/// Avatar (Outfit) selector
/// </summary>
public class WheelSelector : MonoBehaviour
{
    /// <summary>outfit prefabs; fill in editor</summary>
    public List<GameObject> prefabs = new List<GameObject>();

    private List<GameObject> instances = new List<GameObject>(); // instantiated prefabs

    private int current_idx = 0;
    private Vector3 prevPrefabPosition, currPrefabPosition, nextPrefabPosition, prefabsOffCameraPosition;

    public GameObject catalogueCamera;
    private Camera cam;

    private bool isHidden = false;

    ///<summary>ref to avatar controller</summary>
    public AvatarView avatarView;

    void Start() {

        {// save positions of place holders and destroy them
            Transform prevPlaceholder = transform.Find("prevPrefab_placeholder");
            prevPrefabPosition = prevPlaceholder.localPosition;
            GameObject.Destroy(prevPlaceholder.gameObject);

            Transform currPlaceholder = transform.Find("currPrefab_placeholder");
            currPrefabPosition = currPlaceholder.localPosition;
            GameObject.Destroy(currPlaceholder.gameObject);

            Transform nextPlaceholder = transform.Find("nextPrefab_placeholder");
            nextPrefabPosition = nextPlaceholder.localPosition;
            GameObject.Destroy(nextPlaceholder.gameObject);

            Transform prefabsPlaceholder = transform.Find("prefabsPlaceholder(offcamera)");
            prefabsOffCameraPosition = prefabsPlaceholder.localPosition;
            GameObject.Destroy(prefabsPlaceholder.gameObject);
        }

        { //instantiate and store all prefabs in list
            
            foreach(GameObject prefab in prefabs) {
                GameObject obj= GameObject.Instantiate(prefab);
                obj.transform.SetParent(transform);
                obj.transform.localEulerAngles = new Vector3(0, 180, 0);
                obj.transform.localPosition = prefabsOffCameraPosition;

                instances.Add(obj);

                // prefab should have animator component with human like rigged avatar
                //Animator animator = prefab.GetComponent<Animator>();
                //if(animator == null || !animator.avatar.isValid || !animator.avatar.isHuman) {
                //    Debug.LogError("incorrect avatar: " + prefab.name); continue;
                //}

                // change position and move offscreen
            }
        }

        { // set first prefab active
            if(prefabs.Count > 0) {
                current_idx = prefabs.Count - 1;
                rollNext();
            }
        }


        { // find camera
            cam = catalogueCamera.GetComponent<Camera>();
        }

        {
            //if(avatarViewObj == null) throw new Exception("no avatar view set");
            //avatarView = avatarViewObj.GetComponent<AvatarView>();
            if(avatarView == null) throw new Exception("no avatar view set");
        }


        // dont show on start
        hide();
    }

    private void Update() {
        cam.enabled = !isHidden;    
    }

    /// <summary>check if it active (visible)</summary>
    /// <returns>true if selector visible</returns>
    public bool isActive() {
        //return true;
        return !isHidden;
    }

    /// <summary>set visible</summary>
    public void show() {
        isHidden = false;
    }

    /// <summary>set invisible</summary>
    public void hide() {
        isHidden = true;
    }


    /// <summary>switch visible state</summary>
    public void toggle() {
        if(isActive()) hide();
        else show();
    }


    /// <summary>select outfit in the middle and update avatarView</summary>
    public void select() {
        if(prefabs.Count != 0 && prefabs[current_idx] != null) { 
            avatarView.changeAvatar(prefabs[current_idx]);
        }
    }


    /// <summary>roll to the next outfit</summary>
    public void rollNext() {

        if(!isActive()) return;

        { // set prevPrefab back to off camera position
            int prev_idx = prevIdx(current_idx);
            instances[prev_idx].transform.localPosition = prefabsOffCameraPosition;
        }


        // change current index
        current_idx = nextIdx(current_idx);

        // set prev curr and next prefabs to positions
        instances[prevIdx(current_idx)].transform.localPosition = prevPrefabPosition;
        instances[nextIdx(current_idx)].transform.localPosition = nextPrefabPosition;
        instances[current_idx].transform.localPosition = currPrefabPosition;
    }


    /// <summary>roll to the previous outfit</summary>
    public void rollPrev() {

        if(!isActive()) return;

        { // set prevPrefab back to off camera position
            int next_idx = nextIdx(current_idx);
            instances[next_idx].transform.localPosition = prefabsOffCameraPosition;
        }


        // change current index
        current_idx = prevIdx(current_idx);

        // set prev curr and next prefabs to positions
        instances[prevIdx(current_idx)].transform.localPosition = prevPrefabPosition;
        instances[nextIdx(current_idx)].transform.localPosition = nextPrefabPosition;
        instances[current_idx].transform.localPosition = currPrefabPosition;
    }


    private int nextIdx(int idx) {
        idx++;
        if(idx >= prefabs.Count) {
            idx = 0;
        }
        return idx;
    }


    private int prevIdx(int idx) {
        idx--;
        if(idx < 0) {
            idx = prefabs.Count - 1;
        }
        return idx;
    }

}
